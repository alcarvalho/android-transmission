#define PEERID_PREFIX             "-TR2040-"
#define USERAGENT_PREFIX          "2.04"
#define SVN_REVISION              "11151"
#define SVN_REVISION_NUM          11151
#define SHORT_VERSION_STRING      "2.04"
#define LONG_VERSION_STRING       "2.04 (11151)"
#define VERSION_STRING_INFOPLIST  2.04
#define TR_STABLE_RELEASE         1
