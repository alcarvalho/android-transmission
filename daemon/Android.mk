LOCAL_PATH:= $(call my-dir)

#
# transmission-daemon
#

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
 daemon.c watch.c

LOCAL_CFLAGS:= \
	-DEMBEDDED \

LOCAL_C_INCLUDES:= \
	$(LOCAL_PATH)/../ \
	$(LOCAL_PATH)/../third-party \

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libssl

LOCAL_STATIC_LIBRARIES := \
	libtransmission \
	libminiupnp \
	libnatpmp \
	libdht \
	libcurl \
	libevent \
	libz


LOCAL_MODULE:=transmission-daemon

include $(BUILD_EXECUTABLE)


#
# transmission-remote
#

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
 remote.c

LOCAL_CFLAGS:= \
	-DEMBEDDED \


LOCAL_C_INCLUDES:= \
	$(LOCAL_PATH)/../ \
	$(LOCAL_PATH)/../third-party \

LOCAL_SHARED_LIBRARIES := \
	libcutils \
	libssl

LOCAL_STATIC_LIBRARIES := \
	libtransmission \
	libminiupnp \
	libnatpmp \
	libdht \
	libcurl \
	libevent \
	libz


LOCAL_MODULE:=transmission-remote

include $(BUILD_EXECUTABLE)
